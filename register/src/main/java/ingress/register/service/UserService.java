package ingress.register.service;


import ingress.register.dto.CreateDto;
import ingress.register.dto.UserDto;
import ingress.register.entity.User;
import ingress.register.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final ModelMapper modelMapper;

    public UserService(UserRepository userRepository, ModelMapper modelMapper) {
        this.userRepository = userRepository;
        this.modelMapper = modelMapper;
    }

    public User login(UserDto userDto) {
     return null;
     // :(
    }


    public void register(CreateDto createDto) {
        User user = modelMapper.map(createDto, User.class);
        if (!createDto.getPassword().equals(createDto.getPassword2())){
            System.out.println("Passwords must be same");
        }
        userRepository.save(user);
    }
}
