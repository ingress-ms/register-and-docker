package ingress.register.controller;

import ingress.register.dto.CreateDto;
import ingress.register.service.UserService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/car")
public class RegisterController {

    private final UserService userService;

    public RegisterController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/register")
    public void register(@RequestBody CreateDto createDto) {
     userService.register(createDto);
 }
}
